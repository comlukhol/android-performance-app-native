package com.lukhol.android.performance.nativee.fragments;

import com.lukhol.android.R;
import com.lukhol.android.performance.nativee.model.TestCode;

public class SaveToDatabaseFragment extends TestFragment {

    public SaveToDatabaseFragment() {
        super(TestCode.SAVE_TO_DATABASE, R.string.description_test_saveToDatabase);
    }
}