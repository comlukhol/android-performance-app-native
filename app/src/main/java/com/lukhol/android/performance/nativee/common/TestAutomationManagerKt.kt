package com.lukhol.android.performance.nativee.common

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Observer
import android.support.v7.app.AppCompatActivity
import android.util.Log
import androidx.navigation.Navigation
import com.lukhol.android.R
import com.lukhol.android.performance.nativee.model.Resource
import com.lukhol.android.performance.nativee.model.SimpleTest
import com.lukhol.android.performance.nativee.model.Status
import com.lukhol.android.performance.nativee.model.TestCode

class TestAutomationManagerKt(private val activity: AppCompatActivity) {
    private var wasExecuted: Boolean = false
    private val orderedTestCode: Map<TestCode, Int> = linkedMapOf(
            TestCode.ARITHMETIC to R.id.arithmeticFragment,
            TestCode.ARITHMETIC to R.id.arithmeticFragment,
            TestCode.JSON_SERIALIZATION to  R.id.jsonSerializationFragment,
            TestCode.JSON_DESERIALIZATION to R.id.jsonDeserializationFragment,
            TestCode.READ_FROM_DEVICE to  R.id.readFromDeviceFragment,
            TestCode.SAVE_TO_DEVICE to  R.id.saveToDeviceFragment,
            TestCode.SAVE_TO_DATABASE to  R.id.saveToDatabaseFragment,
            TestCode.LOAD_FROM_DATABASE to  R.id.loadFromDatabaseFragment,
            TestCode.READ_LAT_LNG to R.id.readLatLngFragment,
            TestCode.REST_QUERIES to R.id.restQuerisFragment
    )

    fun startAutomation(testsLiveDataDictionary: Map<TestCode, MutableLiveData<Resource<SimpleTest>>>,
                               startTest: BiConsumer<TestCode, AppCompatActivity>,
                               sendSimpleTestResult: Consumer<TestCode>) {

        if(wasExecuted())
            return

        val orderedTestCodeArray = orderedTestCode.keys.toTypedArray()

        for(i in 0..orderedTestCodeArray.size) {
            if(i == orderedTestCodeArray.size - 1)
                break

            val actualTestCode = orderedTestCodeArray[i]
            val nextTestcode = orderedTestCodeArray[i + 1]
            val nextTestFragmentId = orderedTestCode[nextTestcode]

            testsLiveDataDictionary[actualTestCode]?.observe(activity, Observer {
                if(it?.status == Status.SUCCESS) {
                    if(it?.data?.isToSend != false) {
                        sendSimpleTestResult.accept(actualTestCode)
                        android.os.Handler().postDelayed({
                            Navigation.findNavController(activity, R.id.nav_host_fragment).navigate(nextTestFragmentId!!)
                            android.os.Handler().postDelayed({ startTest.accept(nextTestcode, activity) }, 2000)
                        }, 1000)
                    }
                }
            })
        }

        val lastTestcode = orderedTestCodeArray[orderedTestCodeArray.size - 1]
        testsLiveDataDictionary[lastTestcode]?.observe(activity, Observer {
            if(it?.status == Status.SUCCESS) {
                sendSimpleTestResult.accept(lastTestcode)
                Log.i("performance-tests-ended", "performance-tests-ended")
            }
        })

        val firstTestCode = orderedTestCodeArray[0]
        val firstTestFragmentId = orderedTestCode[firstTestCode]
        Navigation.findNavController(activity, R.id.nav_host_fragment).navigate(firstTestFragmentId!!)
        android.os.Handler().postDelayed({ startTest.accept(firstTestCode, activity)}, 2000)
    }

    private fun wasExecuted(): Boolean {
        if(!wasExecuted)
            return false

        wasExecuted = true
        return wasExecuted
    }
}