package com.lukhol.android.performance.nativee.asynctasks;

import android.arch.lifecycle.MutableLiveData;
import android.support.v7.app.AppCompatActivity;

import com.lukhol.android.performance.nativee.model.Resource;
import com.lukhol.android.performance.nativee.model.SimpleTest;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import static android.os.Process.THREAD_PRIORITY_BACKGROUND;
import static android.os.Process.THREAD_PRIORITY_MORE_FAVORABLE;
import static android.os.Process.setThreadPriority;

public class SaveToDeviceAsyncTask extends TestAsyncTask {

    public SaveToDeviceAsyncTask(MutableLiveData<Resource<SimpleTest>> mutableLiveData) {
        super(mutableLiveData);
    }

    @Override
    protected SimpleTest doInBackground(AppCompatActivity... voids) {
        setThreadPriority(THREAD_PRIORITY_BACKGROUND + THREAD_PRIORITY_MORE_FAVORABLE);
        long tStart = System.currentTimeMillis();

        byte[] byteImage = readBytesFromDevice("/sdcard/performanceTests/photos/android-photo-small.jpg");
        deleteDirectoryIfExist("/sdcard/performanceTests/photos/write/");
        createDirectory("/sdcard/performanceTests/photos/write/");

        //Test here
        for(int i = 0 ; i < 100 ; i++) {
            String path = "/sdcard/performanceTests/photos/write/android-photo-small-" + i + ".jpg";
            saveBytesToDevice(path, byteImage);
        }

        long tEnd = System.currentTimeMillis();
        long tDelta = tEnd - tStart;
        return new SimpleTest(tDelta, Double.toString(1), true);
    }

    private void saveBytesToDevice(String path, byte[] bytes) {
        File myFile = new File(path);

        try(FileOutputStream stream = new FileOutputStream(myFile)) {
            stream.write(bytes, 0, bytes.length);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private boolean deleteDirectoryIfExist(String path) {
        File file = new File(path);
        return file.delete();
    }

    private boolean checkIfDirectoryExist(String path) {
        File file = new File(path);
        return file.exists() && file.isDirectory();
    }

    private boolean  createDirectory(String path) {
        if(checkIfDirectoryExist(path)) {
            return true;
        }

        File file = new File(path);
        return file.mkdir();
    }

    private byte[] readBytesFromDevice(String path) {
        File myFile = new File(path);
        byte[] bytes = null;

        try (FileInputStream stream = new FileInputStream(myFile)){
            bytes = new byte[(int)myFile.length()];
            stream.read(bytes);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return bytes;
    }
}