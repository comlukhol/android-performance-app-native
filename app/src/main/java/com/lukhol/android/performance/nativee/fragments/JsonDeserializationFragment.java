package com.lukhol.android.performance.nativee.fragments;

import com.lukhol.android.R;
import com.lukhol.android.performance.nativee.model.TestCode;

public class JsonDeserializationFragment extends TestFragment{

    public JsonDeserializationFragment() {
        super(TestCode.JSON_DESERIALIZATION, R.string.description_test_jsonDeserialization);
    }
}
