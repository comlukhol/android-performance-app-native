package com.lukhol.android.performance.nativee.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.lukhol.android.performance.nativee.model.SampleEntity;

import java.util.List;

@Dao
public interface SampleDao {

    @Query("SELECT * from sample_entity")
    List<SampleEntity> getAll();

    @Insert
    void insertAll(SampleEntity... sampleEntities);

    @Insert
    void insertAll(List<SampleEntity> sampleEntities);

    @Query("select COUNT(*) from sample_entity")
    long getCount();

    @Query("DELETE FROM sample_entity")
    void deleteAll();
}