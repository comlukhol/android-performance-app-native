package com.lukhol.android.performance.nativee.asynctasks

import android.arch.lifecycle.MutableLiveData
import android.os.Looper
import android.os.Process.*
import android.support.v7.app.AppCompatActivity
import com.google.android.gms.location.*
import com.lukhol.android.performance.nativee.CustomApplication
import com.lukhol.android.performance.nativee.MainActivity
import com.lukhol.android.performance.nativee.model.Resource
import com.lukhol.android.performance.nativee.model.SimpleTest


class ReadLatLngAsyncTask(mutableLiveData: MutableLiveData<Resource<SimpleTest>>) : TestAsyncTask(mutableLiveData) {

    private lateinit var locationCallback: LocationCallback
    private lateinit var providerClient: FusedLocationProviderClient
    private lateinit var locationRequest: LocationRequest
    private var tStart: Long = 0
    private lateinit var mainActivity: MainActivity

    override fun onPreExecute() {
        super.onPreExecute()
        tStart = System.currentTimeMillis()

        providerClient = LocationServices.getFusedLocationProviderClient(CustomApplication.application)
        locationRequest = createLocationRequest()

        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                locationResult ?: return
                for (location in locationResult.locations) {
                    val latitude = location?.latitude ?: 0.0
                    val longitude = location?.longitude ?: 0.0

                    val simpleTest = SimpleTest(System.currentTimeMillis() - tStart, "$latitude, $longitude"
                            ?: "", true)
                    providerClient.removeLocationUpdates(locationCallback)
                    mainActivity.extraResult(simpleTest)
                }
            }
        }

        try {
            providerClient.requestLocationUpdates(locationRequest, locationCallback, null)
        } catch (e: SecurityException) {
            e.printStackTrace()
        }
    }

    override fun doInBackground(vararg params: AppCompatActivity?): SimpleTest {
        setThreadPriority(THREAD_PRIORITY_BACKGROUND + THREAD_PRIORITY_MORE_FAVORABLE)
        mainActivity = params[0] as MainActivity

        val tEnd = System.currentTimeMillis()
        val tDelta = tEnd - tStart

        return SimpleTest(tDelta, "", false)
    }

    private fun createLocationRequest(): LocationRequest {
        val locationRequest = LocationRequest()
        locationRequest.interval = 1
        locationRequest.fastestInterval = 1
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        return locationRequest
    }
}