package com.lukhol.android.performance.nativee.common;

import android.app.Activity;
import com.lukhol.android.performance.nativee.MainActivity;

public interface PermissionManager {
    void verifyPermission(Activity activity, int requestCode, String permission);
    void verifyPermissions(Activity activity, int requestCode, String... permissions);
}