package com.lukhol.android.performance.nativee.common;

@FunctionalInterface
public interface Consumer<T> {
    void accept(T t);
}