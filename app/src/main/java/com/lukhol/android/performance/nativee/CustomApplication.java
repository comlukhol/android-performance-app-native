package com.lukhol.android.performance.nativee;

import android.app.Application;

import com.lukhol.android.performance.nativee.dagger.AppModule;
import com.lukhol.android.performance.nativee.dagger.ApplicationComponent;
import com.lukhol.android.performance.nativee.dagger.DaggerApplicationComponent;
import com.lukhol.android.performance.nativee.dagger.WebModule;

public class CustomApplication extends Application {

    public static Application application;
    private ApplicationComponent netComponent;

    public CustomApplication() {
        this.application = this;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        netComponent = DaggerApplicationComponent.builder()
                .appModule(new AppModule(this))
                .webModule(new WebModule("https://360d58ea.ngrok.io"))
                .build();
    }

    public ApplicationComponent getNetComponent() {
        return netComponent;
    }
}