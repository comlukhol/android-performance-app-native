package com.lukhol.android.performance.nativee.model.weather;

public class WeatherDto {
    private Long id;
    private String name;
    private int cod;
    private Coord coord;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCod() {
        return cod;
    }

    public void setCod(int cod) {
        this.cod = cod;
    }

    public Coord getCoord() {
        return coord;
    }

    public void setCoord(Coord coord) {
        this.coord = coord;
    }

    @Override
    public String toString() {
        return "WeatherDto{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", cod=" + cod +
                ", coord=" + coord.toString() +
                '}';
    }
}