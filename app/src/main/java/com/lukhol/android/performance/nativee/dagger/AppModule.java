package com.lukhol.android.performance.nativee.dagger;

import android.app.Application;
import android.arch.persistence.room.Room;

import com.lukhol.android.performance.nativee.common.AppDatabase;
import com.lukhol.android.performance.nativee.common.AsyncTaskProviderImpl;
import com.lukhol.android.performance.nativee.common.AsyncTasksProvider;
import com.lukhol.android.performance.nativee.common.Settings;

import javax.inject.Singleton;

import com.lukhol.android.performance.nativee.common.PermissionManager;
import com.lukhol.android.performance.nativee.common.PermissionManagerImpl;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    private Application application;

    public AppModule(Application application) {
        this.application = application;
    }

    @Provides
    @Singleton
    Application provideApplication() {
        return application;
    }

    @Provides
    @Singleton
    Settings provideSettings() {
        return new Settings();
    }

    @Provides
    @Singleton
    PermissionManager providePermissionManager() {
        return new PermissionManagerImpl();
    }

    @Provides
    @Singleton
    AppDatabase provideAppDatabase() {
        return Room.databaseBuilder(
                application.getApplicationContext(),
                AppDatabase.class,
                "android-performance-tests"
        ).build();
    }
}