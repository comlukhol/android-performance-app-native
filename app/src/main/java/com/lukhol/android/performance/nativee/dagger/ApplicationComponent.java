package com.lukhol.android.performance.nativee.dagger;

import com.lukhol.android.performance.nativee.MainActivity;
import com.lukhol.android.performance.nativee.fragments.ArithmeticFragment;

import javax.inject.Singleton;

import com.lukhol.android.performance.nativee.fragments.JsonSerializationFragment;
import com.lukhol.android.performance.nativee.fragments.TestFragment;
import dagger.Component;

@Singleton
@Component(modules = {AppModule.class, WebModule.class, ViewModelModule.class})
public interface ApplicationComponent {
    void inject(MainActivity mainActivity);
    void inject(TestFragment testFragment);
}