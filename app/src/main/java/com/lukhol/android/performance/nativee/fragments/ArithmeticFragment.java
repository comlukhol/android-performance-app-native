package com.lukhol.android.performance.nativee.fragments;

import com.lukhol.android.R;
import com.lukhol.android.performance.nativee.model.TestCode;

public class ArithmeticFragment extends TestFragment {

    public ArithmeticFragment() {
        super(TestCode.ARITHMETIC,  R.string.description_test_arithmetic);
    }
}