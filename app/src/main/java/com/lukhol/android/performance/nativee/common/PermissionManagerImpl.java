package com.lukhol.android.performance.nativee.common;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import java.util.ArrayList;
import java.util.List;

public class PermissionManagerImpl implements PermissionManager{
    public void verifyPermission(Activity activity, int requestCode, String permission) {
        if (ContextCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)) {

            } else {
                ActivityCompat.requestPermissions(activity, new String[]{permission}, requestCode);
            }
        }
    }

    public void verifyPermissions(Activity activity, int requestCode, String... permissions) {
        List<String> notAllowedPermissions = new ArrayList<>();

        for(String singlePermission : permissions) {
            if (ContextCompat.checkSelfPermission(activity, singlePermission) != PackageManager.PERMISSION_GRANTED)
                notAllowedPermissions.add(singlePermission);
        }

        if(!notAllowedPermissions.isEmpty()) {
            ActivityCompat.requestPermissions(activity, notAllowedPermissions.toArray(new String[notAllowedPermissions.size()]), requestCode);
        }
    }
}