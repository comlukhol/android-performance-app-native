package com.lukhol.android.performance.nativee.asynctasks;

import android.arch.lifecycle.MutableLiveData;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.lukhol.android.performance.nativee.model.Resource;
import com.lukhol.android.performance.nativee.model.SimpleTest;
import com.lukhol.android.performance.nativee.model.weather.WeatherDto;
import com.lukhol.android.performance.nativee.services.WeatherWebService;

import java.io.IOException;

import retrofit2.Response;

public class RestQueryAsyncTask extends TestAsyncTask {

    private final WeatherWebService weatherWebService;

    public RestQueryAsyncTask(MutableLiveData<Resource<SimpleTest>> mutableLiveData, WeatherWebService weatherWebService) {
        super(mutableLiveData);
        this.weatherWebService = weatherWebService;
    }

    @Override
    protected SimpleTest doInBackground(AppCompatActivity... appCompatActivities) {
        long tStart = System.currentTimeMillis();

        WeatherDto weatherDto = null;
        try {
            for(int i = 0 ; i < 5 ; i++) {
                Response<WeatherDto> weatherDtoResponse = weatherWebService.getWeather("Lodz", "a4d092435de7aa0a03d195383861eb7f").execute();
                weatherDto = weatherDtoResponse.body();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        long tEnd = System.currentTimeMillis();
        long tDelta = tEnd - tStart;

        return new SimpleTest(tDelta, weatherDto != null ?weatherDto.toString() : "", true);
    }
}
