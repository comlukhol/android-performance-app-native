package com.lukhol.android.performance.nativee.services;

import com.lukhol.android.performance.nativee.model.weather.WeatherDto;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface WeatherWebService {

    @GET("https://api.openweathermap.org/data/2.5/weather")
    Call<WeatherDto> getWeather(@Query("q") String city, @Query("appid") String appId);
}
