package com.lukhol.android.performance.nativee.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "sample_entity")
public class SampleEntity {

    @PrimaryKey
    private Long id;

    @ColumnInfo(name = "string_content")
    private String stringContent;

    @ColumnInfo(name = "boolean_content")
    private boolean booleanContent;

    @ColumnInfo(name = "double_content")
    private double doubleContent;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStringContent() {
        return stringContent;
    }

    public void setStringContent(String stringContent) {
        this.stringContent = stringContent;
    }

    public boolean isBooleanContent() {
        return booleanContent;
    }

    public void setBooleanContent(boolean booleanContent) {
        this.booleanContent = booleanContent;
    }

    public double getDoubleContent() {
        return doubleContent;
    }

    public void setDoubleContent(double doubleContent) {
        this.doubleContent = doubleContent;
    }
}