package com.lukhol.android.performance.nativee.fragments;

import com.lukhol.android.R;
import com.lukhol.android.performance.nativee.model.TestCode;

public class ReadFromDeviceFragment extends TestFragment {

    public ReadFromDeviceFragment() {
        super(TestCode.READ_FROM_DEVICE, R.string.description_test_readFromDevice);
    }
}