package com.lukhol.android.performance.nativee.asynctasks;

import android.arch.lifecycle.MutableLiveData;
import android.support.v7.app.AppCompatActivity;

import com.lukhol.android.performance.nativee.model.Resource;
import com.lukhol.android.performance.nativee.model.SimpleTest;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static android.os.Process.*;

public class ArithmeticAsyncTask extends TestAsyncTask {

    public ArithmeticAsyncTask(MutableLiveData<Resource<SimpleTest>> mutableLiveData) {
        super(mutableLiveData);
    }

    @Override
    protected SimpleTest doInBackground(AppCompatActivity... voids) {
        setThreadPriority(THREAD_PRIORITY_BACKGROUND + THREAD_PRIORITY_MORE_FAVORABLE);
        long tStart = System.currentTimeMillis();

        String result = piDigits(35000);

        long tEnd = System.currentTimeMillis();
        long tDelta = tEnd - tStart;
        return new SimpleTest(tDelta, result, true);
    }

    private long forLoopOverTenMilionWithAddition() {
        long sum = 0;
        for (int i = 0; i < 10000000; i++) {
            sum += i;
        }

        return sum;
    }

    private String piDigits(int digits){
        final int SCALE = 10000;
        final int ARRINIT = 2000;

        StringBuilder pi = new StringBuilder();
        int[] arr = new int[digits + 1];
        int carry = 0;

        for (int i = 0; i <= digits; ++i)
            arr[i] = ARRINIT;

        for (int i = digits; i > 0; i-= 14) {
            int sum = 0;
            for (int j = i; j > 0; --j) {
                sum = sum * j + SCALE * arr[j];
                arr[j] = sum % (j * 2 - 1);
                sum /= j * 2 - 1;
            }

            pi.append(String.format("%04d", carry + sum / SCALE));
            carry = sum % SCALE;
        }
        return pi.toString();
    }
}