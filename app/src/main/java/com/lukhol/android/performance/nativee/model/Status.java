package com.lukhol.android.performance.nativee.model;

public enum Status {
    SUCCESS,
    ERROR,
    LOADING,
    EMPTY
}
