package com.lukhol.android.performance.nativee.asynctasks;

import android.arch.lifecycle.MutableLiveData;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import com.lukhol.android.performance.nativee.model.Resource;
import com.lukhol.android.performance.nativee.model.SimpleTest;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import static android.os.Process.*;

public class ReadFromDeviceAsyncTask extends TestAsyncTask {

    public ReadFromDeviceAsyncTask(MutableLiveData<Resource<SimpleTest>> mutableLiveData) {
        super(mutableLiveData);
    }

    @Override
    protected SimpleTest doInBackground(AppCompatActivity... voids) {
        setThreadPriority(THREAD_PRIORITY_BACKGROUND + THREAD_PRIORITY_MORE_FAVORABLE);
        long tStart = System.currentTimeMillis();

        //test here
        for(int i = 0 ; i < 100 ; i++) {
            File myFile = new File("/sdcard/performanceTests/photos/android-photo-small.jpg");
            byte[] bytes;
            try (FileInputStream stream = new FileInputStream(myFile)){
                bytes = new byte[(int)myFile.length()];
                stream.read(bytes);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        long tEnd = System.currentTimeMillis();
        long tDelta = tEnd - tStart;
        return new SimpleTest(tDelta, Double.toString(1), true);
    }
}
