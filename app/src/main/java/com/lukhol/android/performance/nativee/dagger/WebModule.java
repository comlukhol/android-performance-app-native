package com.lukhol.android.performance.nativee.dagger;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.lukhol.android.performance.nativee.services.TestWebService;
import com.lukhol.android.performance.nativee.services.WeatherWebService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class WebModule {
    private final String baseUrl;

    public WebModule(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    @Provides
    @Singleton
    Gson provideGson() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES);
        return gsonBuilder.create();
    }

    @Provides
    @Singleton
    Retrofit provideRetrofit(Gson gson) {
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(baseUrl)
                .build();

        return retrofit;
    }

    @Provides
    @Singleton
    TestWebService provideTestWebService(Retrofit retrofit) {
        return retrofit.create(TestWebService.class);
    }

    @Provides
    @Singleton
    WeatherWebService provideWeatherWebService(Retrofit retrofit) {
        return retrofit.create(WeatherWebService.class);
    }
}