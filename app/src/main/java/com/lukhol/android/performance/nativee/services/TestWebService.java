package com.lukhol.android.performance.nativee.services;

import com.lukhol.android.performance.nativee.model.SimpleTestDto;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface TestWebService {

    @POST("/api/v1/test/save")
    Call<Void> sendSimpleTestResult(@Body SimpleTestDto simpleTestDto);
}