package com.lukhol.android.performance.nativee;

import android.Manifest;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.lukhol.android.R;
import com.lukhol.android.performance.nativee.common.AppDatabase;
import com.lukhol.android.performance.nativee.common.PermissionManager;
import com.lukhol.android.performance.nativee.common.Settings;
import com.lukhol.android.performance.nativee.model.Resource;
import com.lukhol.android.performance.nativee.model.SimpleTest;
import com.lukhol.android.performance.nativee.model.TestCode;
import com.lukhol.android.performance.nativee.viewmodels.TestsViewModel;

import javax.inject.Inject;

import androidx.navigation.Navigation;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    @Inject
    Settings settings;

    @Inject
    PermissionManager permissionManager;

    @Inject
    AppDatabase appDatabase;

    @Inject
    protected ViewModelProvider.Factory viewModelFactory;
    protected TestsViewModel testsViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((CustomApplication) getApplication()).getNetComponent().inject(this);
        testsViewModel = ViewModelProviders.of(this, viewModelFactory).get(TestsViewModel.class);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        String testId = getIntent().getStringExtra("myExtra");
        String serverUrl = getIntent().getStringExtra("serverUrl");
        settings.setTestId(testId);
        settings.setServerUrl(serverUrl);

        permissionManager.verifyPermissions(
                this,
                15,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.INTERNET,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
        );

        if(testId != null) {
            testsViewModel.executeAllWithTransition(this);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.i("performance-tests-ended", "performance-tests-ended");
    }

    @Override
    public boolean onSupportNavigateUp() {
        return Navigation.findNavController(this, R.id.nav_host_fragment).navigateUp();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.nav_arithmetic) {
           Navigation.findNavController(this, R.id.nav_host_fragment).navigate(R.id.arithmeticFragment);
        } else if (id == R.id.nav_jsonSerialization) {
            Navigation.findNavController(this, R.id.nav_host_fragment).navigate(R.id.jsonSerializationFragment);
        } else if(id == R.id.nav_jsonDeserialization) {
            Navigation.findNavController(this, R.id.nav_host_fragment).navigate(R.id.jsonDeserializationFragment);
        } else if(id == R.id.nav_readFromDevice) {
            Navigation.findNavController(this, R.id.nav_host_fragment).navigate(R.id.readFromDeviceFragment);
        } else if(id == R.id.nav_saveToDevice) {
            Navigation.findNavController(this, R.id.nav_host_fragment).navigate(R.id.saveToDeviceFragment);
        } else if(id == R.id.nav_saveToDatabase) {
            Navigation.findNavController(this, R.id.nav_host_fragment).navigate(R.id.saveToDatabaseFragment);
        } else if(id == R.id.nav_loadFromDatabase) {
            Navigation.findNavController(this, R.id.nav_host_fragment).navigate(R.id.loadFromDatabaseFragment);
        } else if(id == R.id.nav_readLatLng) {
            Navigation.findNavController(this, R.id.nav_host_fragment).navigate(R.id.readLatLngFragment);
        } else if(id == R.id.nav_restQueris) {
            Navigation.findNavController(this, R.id.nav_host_fragment).navigate(R.id.restQuerisFragment);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        return true;
    }

    public void extraResult(SimpleTest simpleTest) {
        testsViewModel.getTestLiveData(TestCode.READ_LAT_LNG).setValue(Resource.success(simpleTest));
    }
}