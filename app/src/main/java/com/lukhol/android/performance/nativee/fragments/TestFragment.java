package com.lukhol.android.performance.nativee.fragments;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.lukhol.android.R;
import com.lukhol.android.databinding.FragmentArithmeticBinding;
import com.lukhol.android.performance.nativee.CustomApplication;
import com.lukhol.android.performance.nativee.model.SimpleTest;
import com.lukhol.android.performance.nativee.model.TestCode;
import com.lukhol.android.performance.nativee.viewmodels.TestsViewModel;

import javax.inject.Inject;

public abstract class TestFragment extends Fragment {

    private final TestCode testCode;
    private final int testDescriptionStringId;

    public TestFragment(TestCode testCode, int testDescriptionStringId) {
        this.testCode = testCode;
        this.testDescriptionStringId = testDescriptionStringId;
    }

    @Inject
    protected ViewModelProvider.Factory viewModelFactory;
    protected TestsViewModel testsViewModel;
    protected FragmentArithmeticBinding binding;

    protected TextView resultTextView, executionTimeTextView, testTitleTextView, loadingTextView;
    protected View arithmeticLayoutContent, arithmeticLayoutLoader;
    protected Button arithmeticButton;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((CustomApplication) getActivity().getApplication()).getNetComponent().inject(this);
        testsViewModel = ViewModelProviders.of(this.getActivity(), viewModelFactory).get(TestsViewModel.class);

        resultTextView = getActivity().findViewById(R.id.resultTextView);
        executionTimeTextView = getActivity().findViewById(R.id.executionTimeTextView);
        testTitleTextView = getActivity().findViewById(R.id.testTitleTextView);
        loadingTextView = getActivity().findViewById(R.id.loadingTextView);
        arithmeticLayoutContent = getActivity().findViewById(R.id.arithmeticLayoutContent);
        arithmeticLayoutLoader = getActivity().findViewById(R.id.arithmeticLayoutLoader);
        arithmeticButton = getActivity().findViewById(R.id.arithmeticButton);

        testTitleTextView.setText(testDescriptionStringId);
        loadingTextView.setText(testDescriptionStringId);

        testsViewModel.getSimpleTestLiveData(testCode).observe(this, jsonSerializationTestResource -> {
            switch (jsonSerializationTestResource.status) {
                case SUCCESS: {
                    resultTextView.setText("Result: " + jsonSerializationTestResource.data.getResult());
                    executionTimeTextView.setText("Execution time: " + jsonSerializationTestResource.data.getExecutionTime());
                    arithmeticLayoutLoader.setVisibility(View.GONE);
                    arithmeticButton.setEnabled(true);
                    break;
                }

                case LOADING: {
                    arithmeticLayoutLoader.setVisibility(View.VISIBLE);
                    arithmeticButton.setEnabled(false);
                    break;
                }

                case EMPTY: {
                    resultTextView.setText("");
                    executionTimeTextView.setText("");
                    arithmeticLayoutLoader.setVisibility(View.GONE);
                    arithmeticButton.setEnabled(true);
                }
            }
        });

        arithmeticButton.setText(R.string.title_button_start);
        arithmeticButton.setOnClickListener(view -> {
            testsViewModel.startTest(testCode, (AppCompatActivity)getActivity());
        });

        binding.setTestCode(testCode);
        binding.setTestsViewModel(testsViewModel);
        binding.setLifecycleOwner(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_arithmetic, container, false);
        //return inflater.inflate(R.layout.fragment_arithmetic, container, false);
        return binding.getRoot();
    }
}