package com.lukhol.android.performance.nativee.asynctasks;

import android.arch.lifecycle.MutableLiveData;
import android.support.v7.app.AppCompatActivity;

import com.google.gson.Gson;
import com.lukhol.android.performance.nativee.model.JsonSerializationSample;
import com.lukhol.android.performance.nativee.model.Resource;
import com.lukhol.android.performance.nativee.model.SimpleTest;

import static android.os.Process.THREAD_PRIORITY_BACKGROUND;
import static android.os.Process.THREAD_PRIORITY_MORE_FAVORABLE;
import static android.os.Process.setThreadPriority;

public class JsonDeserializationAsyncTask extends TestAsyncTask {

    private final Gson gson = new Gson();

    public JsonDeserializationAsyncTask(MutableLiveData<Resource<SimpleTest>> mutableLiveData) {
        super(mutableLiveData);
    }

    @Override
    protected SimpleTest doInBackground(AppCompatActivity... voids) {
        setThreadPriority(THREAD_PRIORITY_BACKGROUND + THREAD_PRIORITY_MORE_FAVORABLE);

        JsonSerializationSample sample = JsonSerializationSample.createSample();
        String json = gson.toJson(sample);

        long tStart = System.currentTimeMillis();
        execute(json);

        long tEnd = System.currentTimeMillis();
        long tDelta = tEnd - tStart;
        return new SimpleTest(tDelta, "Success", true);
    }

    private void execute(String json) {
        JsonSerializationSample jsonSerializationSample;
        for (int i = 0; i < 100; i++) {
            jsonSerializationSample = gson.fromJson(json, JsonSerializationSample.class);
        }
    }
}