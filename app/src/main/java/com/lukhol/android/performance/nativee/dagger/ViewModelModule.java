package com.lukhol.android.performance.nativee.dagger;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import com.lukhol.android.performance.nativee.viewmodels.TestsViewModel;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class ViewModelModule {

    @Binds
    @IntoMap
    @Singleton
    @ViewModelKey(TestsViewModel.class)
    abstract ViewModel bindArithmeticViewModel(TestsViewModel testsViewModel);

    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(ViewModelFactory factory);
}