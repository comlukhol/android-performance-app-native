package com.lukhol.android.performance.nativee.model;

public class SimpleTest {

    private final long executionTime;
    private final String result;
    private final boolean toSend;

    public SimpleTest(long executionTime, String result, boolean toSend) {
        this.executionTime = executionTime;
        this.result = result;
        this.toSend = toSend;
    }

    public long getExecutionTime() {
        return executionTime;
    }

    public String getResult() {
        return result;
    }

    public boolean isToSend() {
        return toSend;
    }
}