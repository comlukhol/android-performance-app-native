package com.lukhol.android.performance.nativee.fragments;

import com.lukhol.android.R;
import com.lukhol.android.performance.nativee.model.TestCode;

public class SaveToDeviceFragment extends TestFragment {

    public SaveToDeviceFragment() {
        super(TestCode.SAVE_TO_DEVICE, R.string.description_test_saveToDevice);
    }
}
