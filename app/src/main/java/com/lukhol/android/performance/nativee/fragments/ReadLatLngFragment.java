package com.lukhol.android.performance.nativee.fragments;

import com.lukhol.android.R;
import com.lukhol.android.performance.nativee.model.SimpleTest;
import com.lukhol.android.performance.nativee.model.TestCode;

public class ReadLatLngFragment extends TestFragment {

    public ReadLatLngFragment() {
        super(TestCode.READ_LAT_LNG, R.string.description_test_readLatLng);
    }
}
