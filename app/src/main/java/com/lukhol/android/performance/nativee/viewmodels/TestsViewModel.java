package com.lukhol.android.performance.nativee.viewmodels;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.support.v7.app.AppCompatActivity;

import com.lukhol.android.performance.nativee.asynctasks.ArithmeticAsyncTask;
import com.lukhol.android.performance.nativee.asynctasks.ArithmeticAsyncTaskKt;
import com.lukhol.android.performance.nativee.asynctasks.JsonDeserializationAsyncTask;
import com.lukhol.android.performance.nativee.asynctasks.JsonSerializationAsyncTask;
import com.lukhol.android.performance.nativee.asynctasks.ReadFromDatabaseAsyncTask;
import com.lukhol.android.performance.nativee.asynctasks.ReadFromDeviceAsyncTask;
import com.lukhol.android.performance.nativee.asynctasks.ReadLatLngAsyncTask;
import com.lukhol.android.performance.nativee.asynctasks.RestQueryAsyncTask;
import com.lukhol.android.performance.nativee.asynctasks.SaveToDatabaseAsyncTask;
import com.lukhol.android.performance.nativee.asynctasks.SaveToDeviceAsyncTask;
import com.lukhol.android.performance.nativee.asynctasks.TestAsyncTask;
import com.lukhol.android.performance.nativee.common.AppDatabase;
import com.lukhol.android.performance.nativee.common.Settings;
import com.lukhol.android.performance.nativee.common.TestAutomationManagerKt;
import com.lukhol.android.performance.nativee.model.Resource;
import com.lukhol.android.performance.nativee.model.SimpleTest;
import com.lukhol.android.performance.nativee.model.SimpleTestDto;
import com.lukhol.android.performance.nativee.model.TestCode;
import com.lukhol.android.performance.nativee.repository.TestRepository;
import com.lukhol.android.performance.nativee.services.TestWebService;
import com.lukhol.android.performance.nativee.services.WeatherWebService;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class TestsViewModel extends ViewModel {

    private final Settings settings;
    private final TestRepository testRepository;
    private final AppDatabase appDatabase;
    private final Map<TestCode, MutableLiveData<Resource<SimpleTest>>> testsLiveDataDictionary;
    private final Map<TestCode, Class> testAsyncTaskClasses = new HashMap<>();
    private final WeatherWebService weatherWebService;

    @Inject
    public TestsViewModel(TestRepository testRepository, Settings settings, AppDatabase appDatabase, WeatherWebService weatherWebService) {
        this.settings = settings;
        this.testRepository = testRepository;
        this.appDatabase = appDatabase;
        this.weatherWebService = weatherWebService;
        testsLiveDataDictionary = new HashMap<>();

        for(TestCode testCode : TestCode.values()) {
            putEmptyLiveDataForCode(testCode);
        }

        testAsyncTaskClasses.put(TestCode.ARITHMETIC, ArithmeticAsyncTask.class);
        testAsyncTaskClasses.put(TestCode.JSON_SERIALIZATION, JsonSerializationAsyncTask.class);
        testAsyncTaskClasses.put(TestCode.JSON_DESERIALIZATION, JsonDeserializationAsyncTask.class);
        testAsyncTaskClasses.put(TestCode.READ_FROM_DEVICE, ReadFromDeviceAsyncTask.class);
        testAsyncTaskClasses.put(TestCode.SAVE_TO_DEVICE, SaveToDeviceAsyncTask.class);
        testAsyncTaskClasses.put(TestCode.SAVE_TO_DATABASE, SaveToDatabaseAsyncTask.class);
        testAsyncTaskClasses.put(TestCode.LOAD_FROM_DATABASE, ReadFromDatabaseAsyncTask.class);
        testAsyncTaskClasses.put(TestCode.READ_LAT_LNG, ReadLatLngAsyncTask.class);
        testAsyncTaskClasses.put(TestCode.REST_QUERIES, RestQueryAsyncTask.class);
    }

    public MutableLiveData<Resource<SimpleTest>> getTestLiveData(TestCode testCode) {
        return testsLiveDataDictionary.get(testCode);
    }

    public LiveData<Resource<SimpleTest>> getSimpleTestLiveData(TestCode testCode) {
        return testsLiveDataDictionary.get(testCode);
    }

    public void startTest(TestCode testCode, AppCompatActivity activity) {
        MutableLiveData<Resource<SimpleTest>> testLiveData = testsLiveDataDictionary.get(testCode);
        testLiveData.setValue(Resource.loading(null));

        try {
            Class clazz = testAsyncTaskClasses.get(testCode);
            TestAsyncTask testAsyncTask = null;

            for(Constructor<?> constructor : clazz.getConstructors()) {
                if(constructor.getParameterTypes().length == 1) {
                    testAsyncTask = (TestAsyncTask) constructor.newInstance(testLiveData);
                    break;
                } else if(constructor.getParameterTypes().length == 2){
                    int count = 0;
                    for(Class<?> clz : constructor.getParameterTypes()) {
                        if(count++ == 1) {
                            if(clz.equals(AppDatabase.class)){
                                testAsyncTask = (TestAsyncTask) constructor.newInstance(testLiveData, appDatabase);
                            } else if (clz.equals(WeatherWebService.class)) {
                                testAsyncTask = (TestAsyncTask) constructor.newInstance(testLiveData, weatherWebService);
                            }
                        }
                    }
                    break;
                }
            }

            testAsyncTask.execute(activity);
        } catch(InvocationTargetException|InstantiationException|IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public void sendSimpleTestResult(TestCode testCode) {
        SimpleTest simpleTest = testsLiveDataDictionary.get(testCode).getValue().data;
        if(simpleTest == null || settings.getTestId() == null || settings.getTestId().equals(""))
            return;

        SimpleTestDto simpleTestDto = new SimpleTestDto();

        simpleTestDto.setTestCode(testCode);
        simpleTestDto.setTime(simpleTest.getExecutionTime());
        simpleTestDto.setTestId(settings.getTestId());

        testRepository.sendSimpleTestResult(simpleTestDto);
    }

    public void executeAllWithTransition(AppCompatActivity activity) {
        TestAutomationManagerKt tam = new TestAutomationManagerKt(activity);
        tam.startAutomation(testsLiveDataDictionary, this::startTest, this::sendSimpleTestResult);
    }

    private void putEmptyLiveDataForCode(TestCode testCode) {
        MutableLiveData<Resource<SimpleTest>> arithmeticLiveData = new MutableLiveData<>();
        arithmeticLiveData.setValue(Resource.empty());
        testsLiveDataDictionary.put(testCode, arithmeticLiveData);
    }
}