package com.lukhol.android.performance.nativee.model;

public class SimpleTestDto {
    private String testId;
    private TestCode testCode;
    private final String applicationType = "NATIVE";
    private long time;

    public String getTestId() {
        return testId;
    }

    public void setTestId(String testId) {
        this.testId = testId;
    }

    public TestCode getTestCode() {
        return testCode;
    }

    public void setTestCode(TestCode testCode) {
        this.testCode = testCode;
    }

    public String getApplicationType() {
        return applicationType;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }
}