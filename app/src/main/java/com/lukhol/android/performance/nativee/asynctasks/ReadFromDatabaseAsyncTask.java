package com.lukhol.android.performance.nativee.asynctasks;

import android.arch.lifecycle.MutableLiveData;
import android.support.v7.app.AppCompatActivity;

import com.lukhol.android.performance.nativee.common.AppDatabase;
import com.lukhol.android.performance.nativee.model.Resource;
import com.lukhol.android.performance.nativee.model.SampleEntity;
import com.lukhol.android.performance.nativee.model.SimpleTest;

import java.util.ArrayList;
import java.util.List;

import static android.os.Process.THREAD_PRIORITY_BACKGROUND;
import static android.os.Process.THREAD_PRIORITY_MORE_FAVORABLE;
import static android.os.Process.setThreadPriority;

public class ReadFromDatabaseAsyncTask extends TestAsyncTask {

    private final AppDatabase appDatabase;

    public ReadFromDatabaseAsyncTask(MutableLiveData<Resource<SimpleTest>> mutableLiveData, AppDatabase appDatabase) {
        super(mutableLiveData);
        this.appDatabase = appDatabase;
    }

    @Override
    protected SimpleTest doInBackground(AppCompatActivity... voids) {
        setThreadPriority(THREAD_PRIORITY_BACKGROUND + THREAD_PRIORITY_MORE_FAVORABLE);
        appDatabase.sampleDao().deleteAll();
        appDatabase.sampleDao().insertAll(prepareSampleEntities());

        long tStart = System.currentTimeMillis();

        List<SampleEntity> sampleEntities = appDatabase.sampleDao().getAll();

        long tEnd = System.currentTimeMillis();
        long tDelta = tEnd - tStart;

        return new SimpleTest(tDelta, Integer.toString(sampleEntities.size()), true);
    }

    private List<SampleEntity> prepareSampleEntities() {
        List<SampleEntity> sampleEntities = new ArrayList<>();
        for(int i = 0 ; i < 10000 ; i++) {
            SampleEntity sampleEntity = new SampleEntity();
            sampleEntity.setBooleanContent(true);
            sampleEntity.setDoubleContent(1515.114465654);
            sampleEntity.setStringContent("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin vel malesuada urna. Interdum et malesuada fames ac ante ipsum primis in faucibus. Aenean varius tincidunt eleifend. Sed semper urna vehicula condimentum consequat. Proin in malesuada nisi. Nam et pretium arcu, ut rutrum felis. Nunc laoreet pharetra nisl luctus rutrum. Mauris ex odio, facilisis et ultricies vel, gravida eu ante. Phasellus bibendum ex eu pellentesque egestas. Etiam nec dictum sapien, eget molestie lectus. Aenean lorem leo, commodo ut sodales et, rutrum eu quam. Proin gravida, ex vitae tincidunt viverra, orci augue consectetur quam, nec dignissim nunc diam egestas lorem. Nullam rutrum cursus diam, eu pretium odio elementum eu. Nam quis consequat diam. Interdum et malesuada fames ac ante ipsum primis in faucibus. Sed faucibus accumsan nibh, quis faucibus elit tempor sed. Maecenas id felis a lectus euismod volutpat. Sed gravida mollis tristique. Donec nec gravida urna. Nunc nec urna in odio volutpat accumsan. Suspendisse vel odio metus.");
            sampleEntities.add(sampleEntity);
        }

        return sampleEntities;
    }
}