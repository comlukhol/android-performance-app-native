package com.lukhol.android.performance.nativee.repository;

import android.util.Log;

import com.lukhol.android.performance.nativee.common.Settings;
import com.lukhol.android.performance.nativee.model.SimpleTestDto;
import com.lukhol.android.performance.nativee.services.TestWebService;

import javax.inject.Inject;
import javax.inject.Singleton;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

@Singleton
public class TestRepository {

    private Retrofit retrofit;
    private final Settings settings;
    private TestWebService testWebService;

    @Inject
    public TestRepository(Retrofit retrofit, Settings settings, TestWebService testWebService) {
        this.retrofit = retrofit;
        this.settings = settings;
        this.testWebService = testWebService;
    }

    public void sendSimpleTestResult(SimpleTestDto simpleTestDto) {
        if(!retrofit.baseUrl().equals(settings.getServerUrl())) {
            retrofit = retrofit.newBuilder().baseUrl(settings.getServerUrl()).build();
            testWebService = retrofit.create(TestWebService.class);
        }

        testWebService.sendSimpleTestResult(simpleTestDto).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.d("WEB_SEND", "SUCCESS");
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.d("WEB_SEND", "FAILED");
            }
        });
    }
}