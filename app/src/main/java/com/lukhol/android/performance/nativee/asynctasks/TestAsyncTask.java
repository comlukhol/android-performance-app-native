package com.lukhol.android.performance.nativee.asynctasks;

import android.arch.lifecycle.MutableLiveData;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;

import com.lukhol.android.performance.nativee.model.Resource;
import com.lukhol.android.performance.nativee.model.SimpleTest;

public abstract class TestAsyncTask extends AsyncTask<AppCompatActivity, Void, SimpleTest> {

    protected final MutableLiveData<Resource<SimpleTest>> mutableLiveData;

    public TestAsyncTask(MutableLiveData<Resource<SimpleTest>> mutableLiveData) {
        this.mutableLiveData = mutableLiveData;
    }

    @Override
    protected void onPostExecute(SimpleTest result) {
        this.mutableLiveData.setValue(Resource.success(result));
    }
}