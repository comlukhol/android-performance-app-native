package com.lukhol.android.performance.nativee.common;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.lukhol.android.performance.nativee.dao.SampleDao;
import com.lukhol.android.performance.nativee.model.SampleEntity;

@Database(entities = {SampleEntity.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract SampleDao sampleDao();
}