package com.lukhol.android.performance.nativee.fragments;

import com.lukhol.android.R;
import com.lukhol.android.performance.nativee.model.TestCode;

public class JsonSerializationFragment extends TestFragment {

    public JsonSerializationFragment() {
        super(TestCode.JSON_SERIALIZATION, R.string.description_test_jsonSerialization);
    }
}