package com.lukhol.android.performance.nativee.fragments;

import com.lukhol.android.R;
import com.lukhol.android.performance.nativee.model.TestCode;

public class LoadFromDatabaseFragment extends TestFragment {

    public LoadFromDatabaseFragment() {
        super(TestCode.LOAD_FROM_DATABASE, R.string.description_test_loadFromDatabase);
    }
}