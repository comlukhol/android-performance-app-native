package com.lukhol.android.performance.nativee.common;

import android.arch.lifecycle.MutableLiveData;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.lukhol.android.R;
import com.lukhol.android.performance.nativee.model.Resource;
import com.lukhol.android.performance.nativee.model.SimpleTest;
import com.lukhol.android.performance.nativee.model.Status;
import com.lukhol.android.performance.nativee.model.TestCode;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

import androidx.navigation.Navigation;

public class TestAutomationManager {

    private final AppCompatActivity activity;
    private final Map<TestCode, Integer> orderedTestCode = new LinkedHashMap<>();
    private boolean wasExecuted = false;

    public TestAutomationManager(AppCompatActivity appCompatActivity) {
        this.activity = appCompatActivity;
        orderedTestCode.put(TestCode.ARITHMETIC, R.id.arithmeticFragment);
        orderedTestCode.put(TestCode.JSON_SERIALIZATION, R.id.jsonSerializationFragment);
        orderedTestCode.put(TestCode.JSON_DESERIALIZATION, R.id.jsonDeserializationFragment);
        orderedTestCode.put(TestCode.READ_FROM_DEVICE, R.id.readFromDeviceFragment);
        orderedTestCode.put(TestCode.SAVE_TO_DEVICE, R.id.saveToDeviceFragment);
        orderedTestCode.put(TestCode.SAVE_TO_DATABASE, R.id.saveToDatabaseFragment);
        orderedTestCode.put(TestCode.LOAD_FROM_DATABASE, R.id.loadFromDatabaseFragment);
    }

    public void startAutomation(Map<TestCode, MutableLiveData<Resource<SimpleTest>>> testsLiveDataDictionary,
                                Consumer<TestCode> startTest,
                                Consumer<TestCode> sendSimpleTestResult) {
        if (wasExecuted())
            return;

        TestCode[] orderedTestCodeArray = orderedTestCode.keySet().toArray(new TestCode[orderedTestCode.size()]);

        for (int i = 0; i < orderedTestCodeArray.length; i++) {
            if (i == orderedTestCodeArray.length - 1)
                break;

            TestCode actualTestCode = orderedTestCodeArray[i];
            TestCode nextTestCode = orderedTestCodeArray[i + 1];
            int nextTestFragmentId = orderedTestCode.get(nextTestCode);

            testsLiveDataDictionary.get(actualTestCode).observe(activity, result -> {
                if (result.status == Status.SUCCESS) {
                    sendSimpleTestResult.accept(actualTestCode);
                    new android.os.Handler().postDelayed(() -> {
                        Navigation.findNavController(activity, R.id.nav_host_fragment).navigate(nextTestFragmentId);
                        new android.os.Handler().postDelayed(() -> startTest.accept(nextTestCode), 2000);
                    }, 1000);
                }
            });
        }

        final TestCode lastTestCode = orderedTestCodeArray[orderedTestCodeArray.length - 1];
        testsLiveDataDictionary.get(lastTestCode).observe(activity, result -> {
            if (result.status == Status.SUCCESS) {
                sendSimpleTestResult.accept(lastTestCode);
                Log.i("performance-tests-ended", "performance-tests-ended");
            }
        });

        final TestCode firstTestCode = orderedTestCodeArray[0];
        Navigation.findNavController(activity, R.id.nav_host_fragment).navigate(orderedTestCode.get(firstTestCode));
        new android.os.Handler().postDelayed(() -> startTest.accept(firstTestCode), 2000);
    }

    private boolean wasExecuted() {
        if (!wasExecuted)
            return false;

        wasExecuted = true;
        return wasExecuted;
    }
}