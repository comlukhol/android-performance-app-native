package com.lukhol.android.performance.nativee.asynctasks

import android.arch.lifecycle.MutableLiveData
import android.os.Process
import android.support.v7.app.AppCompatActivity
import com.lukhol.android.performance.nativee.model.Resource
import com.lukhol.android.performance.nativee.model.SimpleTest

class ArithmeticAsyncTaskKt(mutableLiveData: MutableLiveData<Resource<SimpleTest>>) : TestAsyncTask(mutableLiveData) {
    override fun doInBackground(vararg params: AppCompatActivity?): SimpleTest {
        Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND + Process.THREAD_PRIORITY_MORE_FAVORABLE)

        val tStart = System.currentTimeMillis()

        val x = forLoopOverTenMillionWithAddition()
        val tEnd = System.currentTimeMillis()
        val tDelta = tEnd - tStart

        return SimpleTest(tDelta, x.toString(), true)
    }

    private fun forLoopOverTenMillionWithAddition() : Long {
        var sum: Long = 0
        for(i in 0 until 10000000)
            sum += i

        return sum
    }
}