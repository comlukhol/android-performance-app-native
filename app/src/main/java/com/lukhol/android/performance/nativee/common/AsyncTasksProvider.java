package com.lukhol.android.performance.nativee.common;

public interface AsyncTasksProvider {
    Object getTestAsyncTask(Class clazz);
}